const express = require('express');
const router = express.Router();
const controller = require('../controllers/loginController');
// router.get('/', controller.get);
router.post('/auth', controller.post);
// router.put('/:id', controller.put);
// router.delete('/:id', controller.delet);
module.exports = router;
