const express = require('express');
const router = express.Router();
const controller = require('../controllers/inscricaoController');
router.get('/:email', controller.get);
router.get('/', controller.get);

router.post('/', controller.post);
// router.put('/:id', controller.put);
// router.delete('/:id', controller.delet);
module.exports = router;
