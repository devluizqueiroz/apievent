//https://imasters.com.br/front-end/criando-uma-api-node-em-10-passos-com-express-js 
//https://www.jenniferbland.com/saving-data-to-mongodb-database-from-node-js-application-tutorial/
//referencia


const express = require('express');
const app = express();
const router = express.Router();
const index = require('./routes/index');
const clienteRoute = require('./routes/clienteRoutes');
const loginRoute = require('./routes/loginRoutes');
const eventoRoute = require('./routes/eventoRoutes');
const inscricaoRoute = require('./routes/inscricaoRoutes');


const bodyParser = require('body-parser');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', index);
app.use('/cliente', clienteRoute);
app.use('/login', loginRoute);
app.use('/evento', eventoRoute);
app.use('/inscricao', inscricaoRoute);



module.exports = app;